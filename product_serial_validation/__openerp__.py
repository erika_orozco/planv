# -*- coding: utf-8 -*-
{
    'name': "Product Serial Validation",
     "description": "Valida si un producto acepta series o no",

    'author': "Erika Orozco - Tantums",

    'category': 'Tools',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','product'],

    # always loaded
    'data': [
        'product_serial_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [ ],
}