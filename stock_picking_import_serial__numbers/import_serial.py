# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2014 Noviat nv/sa (www.noviat.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

#import time
import base64, StringIO, csv
from openerp.osv import orm, fields
from openerp.addons.decimal_precision import decimal_precision as dp
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)
import pdb

header_fields = ['serie', 'producto']

class picking_serial_import(orm.TransientModel):
	_name = 'picking.serial.import'
	_description = 'Import serial numbers'

	_columns = {
		'aml_data': fields.binary('File', required=True),
		'aml_fname': fields.char('Filename', size=128, required=True),
		'csv_separator': fields.selection([(',',','),(';',';')], 'CSV Separator', required=True),
		'decimal_separator': fields.selection([('.','.'),(',',',')], 'Decimal Separator', required=False),
		'note':fields.text('Log'),
	}
	_defaults = {
		'aml_fname': '',
		'csv_separator': ',',
	}

	def do_import(self, cr, uid, ids, context=None):
		data = self.browse(cr,uid,ids)[0]
		aml_file = data.aml_data
		aml_filename = data.aml_fname
		csv_separator = str(data.csv_separator)
		#decimal_separator = data.decimal_separator
		product_tmp_obj = self.pool.get('product.product')
		production_lot_obj = self.pool.get('stock.production.lot')
		product_id = 0
		picking_name = context['stock_picking_name']
		err_log = ''
		header_line = False

		lines = base64.decodestring(aml_file)
		reader = csv.reader(StringIO.StringIO(lines), delimiter=csv_separator)
		amls = []
		#pdb.set_trace()
		row_cnt = 0
		for ln in reader:
			row_cnt = row_cnt + 1

		row_cnt = row_cnt - 1
		if row_cnt != context['total_movs']:
			raise orm.except_orm(_('Error :'), _("Error while processing the file. \n\nEl archivo contiene mayor o menor numeros de series, necesita %s") %context['total_movs'])

		reader = csv.reader(StringIO.StringIO(lines), delimiter=csv_separator)
		for ln in reader:
			# process header line
			serial_number = ln[0]
			product_code = ln[1]
			if not header_line:
				if serial_number.strip().lower() not in header_fields and product_code.strip().lower() not in header_fields:
					raise orm.except_orm(_('Error :'), _("Error while processing the header line %s. \n\nPlease check your CSV separator as well as the column header fields") %ln)
				else:
					header_line = True
			else:
				if product_code not in context['product_name_list']:
					raise orm.except_orm(_('Error :'), _("El codigo del articulo %s en el archvo no coicide con alguno de los productos a transferir") %product_code)
				else:
					product_id = product_tmp_obj.search(cr,uid,[('default_code','=',product_code)])[0]
					production_lot_id = production_lot_obj.search(cr,uid,[('name','=',serial_number),('product_id','=',product_id),('origin_document','=',picking_name)])
					#pdb.set_trace()
					if production_lot_id:
						production_lot_obj.unlink(cr,uid,production_lot_id)

					production_lot_obj.create(cr,uid,{'name': serial_number, 'product_id': product_id, 'origin_document': picking_name})
				#err_log += '\n' + _("Invalid CSV File, Header Field '%s' is not supported !") %ln[i]			
		return True