��    !      $  /   ,      �     �     �     �  
     
             ,  Z   =     �     �  c   �  p   
     {     �     �     �     �     �  K   �               '     7  
   ;     F     S  	   b     l  
     �   �     +     B  V  E     �     �     �  
   �  
   �     �     �  Z   �     N     T  k   \  �   �     |	     �	     �	     �	     �	     �	  K   �	     
     4
     D
     T
     X
     j
     s
  	   �
     �
     �
  �   �
     e     |                                                    !                                   
                         	                                               CSV Separator Cancel Close Created by Created on Decimal Separator Documento Origen El codigo del articulo %s en el archvo no coicide con alguno de los productos a transferir Error Error : Error while processing the file. 

El archivo contiene mayor o menor numeros de series, necesita %s Error while processing the header line %s. 

Please check your CSV separator as well as the column header fields File Filename ID Import Import File Import Serial Numbers Import not allowed when not exist move lines. 
Please add move lines first. Import serial numbers Last Updated by Last Updated on Log Lot/Serial Picking List Picking wizard Results : Select your file : Stock Move The roundings of your Unit of Measures %s on the move vs. %s on the product don't allow to do these operations or you are not transferring the picking at once.  Unsupported Function : or Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-02 17:45+0000
PO-Revision-Date: 2015-05-02 12:55-0600
Last-Translator: Cesar equihua <cequihua@tantums.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Language: es_MX
 CSV Separador Cancelar Cerrar Created by Created on Decimal Separator Documento Origen El codigo del articulo %s en el archvo no coicide con alguno de los productos a transferir Error Error : Error mientras se procesaba el archivo. 

El archivo contiene mayor o menor números de series, necesita %s Error mientras se procesaban los encabezados del archivo %s. 

Por favor revise su CSV separador, la primera fila son las columnas del archivo y deben de nombrarse serie, producto Archivo Nombre del Archivo ID Importar Importar Archivo Importar Numeros de Serie Import not allowed when not exist move lines. 
Please add move lines first. Importar numeros de serie Last Updated by Last Updated on Log Lote/Nº de serie Albarán Asistente de albarán Results : Seleccione su archivo: Movimiento de existencias The roundings of your Unit of Measures %s on the move vs. %s on the product don't allow to do these operations or you are not transferring the picking at once.  Unsupported Function : or 