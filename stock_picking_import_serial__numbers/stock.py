# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.odoo.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import except_orm
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)
import time
import pdb

class stock_production_lot(models.Model):
	_inherit = 'stock.production.lot'
	origin_document = fields.Char(string="Documento Origen")

class stock_picking(models.Model):
	_inherit = 'stock.picking'

	def import_serial(self, cr, uid, ids, context=None):
		mod_obj = self.pool.get('ir.model.data')
		wiz_view = mod_obj.get_object_reference(cr, uid, 'stock_picking_import_serial__numbers', 'picking_serial_import_view')
		for move in self.browse(cr, uid, ids, context=context):
			if not move.move_lines:
				raise except_orm(_('Unsupported Function :'),
					_("Import not allowed when not exist move lines. \nPlease add move lines first."))
			
			total_movs = 0
			product_name_list = []

			for ml in move.move_lines:
				total_movs = total_movs + ml.product_uom_qty
				product_name_list.append(ml.product_id.default_code)

			ctx = {
				'stock_picking_id': move.id,
				'total_movs': total_movs,
				'stock_picking_name': move.name,
				'product_name_list': product_name_list,
			}
			act_import = {
				'name':'Import File',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'picking.serial.import',
				'view_id': [wiz_view[1]],
				'nodestroy': True,
				'target': 'new',
				'type': 'ir.actions.act_window',
				'context': ctx,
			}
			return act_import
		

class stock_transfer_details(models.TransientModel):
	_inherit = 'stock.transfer_details'

	def default_get(self, cr, uid, fields, context=None):
		if context is None: context = {}
		res = super(stock_transfer_details, self).default_get(cr, uid, fields, context=context)
		picking_ids = context.get('active_ids', [])
		active_model = context.get('active_model')

		if not picking_ids or len(picking_ids) != 1:
		    # Partial Picking Processing may only be done for one picking at a time
		    return res
		assert active_model in ('stock.picking'), 'Bad context propagation'
		picking_id, = picking_ids
		picking = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
		items = []
		packs = []
		if not picking.pack_operation_ids:
		    picking.do_prepare_partial()
		for op in picking.pack_operation_ids:
			print picking.name
			stock_product_lot_ids = self.pool.get('stock.production.lot').search(cr,uid,[('product_id','=',op.product_id.id),('origin_document','=',picking.name)])
			op_id = op.id
			op_product_id = op.product_id.id
			op_product_uom_id = op.product_uom_id.id
			op_package_id_id = op.package_id.id
			op_location_id = op.location_id.id
			op_location_dest_id = op.location_dest_id.id
			op_result_package_id = op.result_package_id.id
			op_date = op.date
			op_owner_id = op.owner_id.id

			if stock_product_lot_ids:
				for lot_id in stock_product_lot_ids:
					item = {
			        	'packop_id': False,
			        	'product_id': op_product_id,
				        'product_uom_id': op_product_uom_id,
				        'quantity': 1,
				        'package_id': op_package_id_id,
				        'lot_id': lot_id,
				        'sourceloc_id': op_location_id,
				        'destinationloc_id': op_location_dest_id,
				        'result_package_id': op_result_package_id,
				        'date': op_date, 
				        'owner_id': op_owner_id,
			    	}

					if op.product_id:
						items.append(item)
					elif op.package_id:
						packs.append(item)
			else:
			    item = {
			        'packop_id': op.id,
			        'product_id': op.product_id.id,
			        'product_uom_id': op.product_uom_id.id,
			        'quantity': op.product_qty,
			        'package_id': op.package_id.id,
			        'lot_id': op.lot_id.id,
			        'sourceloc_id': op.location_id.id,
			        'destinationloc_id': op.location_dest_id.id,
			        'result_package_id': op.result_package_id.id,
			        'date': op.date, 
			        'owner_id': op.owner_id.id,
			    }
			    if op.product_id:
			        items.append(item)
			    elif op.package_id:
			        packs.append(item)
		res.update(item_ids=items)
		res.update(packop_ids=packs)
		return res

	@api.one
	def do_detailed_transfer(self):
		processed_ids = []
		# Create new and update existing pack operations
		for lstits in [self.item_ids, self.packop_ids]:
			for prod in lstits:
				pack_datas = {
					'product_id': prod.product_id.id,
					'product_uom_id': prod.product_uom_id.id,
					'product_qty': prod.quantity,
					'package_id': prod.package_id.id,
					'lot_id': prod.lot_id.id,
					'location_id': prod.sourceloc_id.id,
					'location_dest_id': prod.destinationloc_id.id,
					'result_package_id': prod.result_package_id.id,
					'date': prod.date if prod.date else datetime.now(),
					'owner_id': prod.owner_id.id,
				}

				if prod.packop_id:
					prod.packop_id.write(pack_datas)
					processed_ids.append(prod.packop_id.id)
				else:
					pack_datas['picking_id'] = self.picking_id.id
					packop_id = self.env['stock.pack.operation'].create(pack_datas)
					processed_ids.append(packop_id.id)
		# Delete the others
		packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
		for packop in packops:
			packop.unlink()

		# Execute the transfer of the picking
		self.picking_id.do_transfer()
		return True

class stock_picking(models.Model):
	_inherit = "stock.picking"

	@api.cr_uid_ids_context
	def do_transfer(self, cr, uid, picking_ids, context=None):
		if not context:
			context = {}
		stock_move_obj = self.pool.get('stock.move')
		for picking in self.browse(cr, uid, picking_ids, context=context):
			if not picking.pack_operation_ids:
				self.action_done(cr, uid, [picking.id], context=context)
				continue
			else:
				need_rereserve, all_op_processed = self.picking_recompute_remaining_quantities(cr, uid, picking, context=context)
				#create extra moves in the picking (unexpected product moves coming from pack operations)
				todo_move_ids = []
				if not all_op_processed:
					todo_move_ids += self._create_extra_moves(cr, uid, picking, context=context)

				#split move lines if needed
				toassign_move_ids = []
				for move in picking.move_lines:
					remaining_qty = move.remaining_qty
					if move.state in ('done', 'cancel'):
						#ignore stock moves cancelled or already done
						continue
					elif move.state == 'draft':
						toassign_move_ids.append(move.id)
					if float_compare(remaining_qty, 0,  precision_rounding = move.product_id.uom_id.rounding) == 0:
						if move.state in ('draft', 'assigned', 'confirmed'):
							todo_move_ids.append(move.id)
					elif float_compare(remaining_qty,0, precision_rounding = move.product_id.uom_id.rounding) > 0 and \
							float_compare(remaining_qty, move.product_qty, precision_rounding = move.product_id.uom_id.rounding) < 0:
						new_move = stock_move_obj.split(cr, uid, move, remaining_qty, context=context)
						todo_move_ids.append(move.id)
						#Assign move as it was assigned before
						toassign_move_ids.append(new_move)
				if need_rereserve or not all_op_processed: 
					if not picking.location_id.usage in ("supplier", "production", "inventory"):
						self.rereserve_quants(cr, uid, picking, move_ids=todo_move_ids, context=context)
					self.do_recompute_remaining_quantities(cr, uid, [picking.id], context=context)
				if todo_move_ids and not context.get('do_only_split'):
					self.pool.get('stock.move').action_done(cr, uid, todo_move_ids, context=context)
				elif context.get('do_only_split'):
					context = dict(context, split=todo_move_ids)
			self._create_backorder(cr, uid, picking, context=context)
			if toassign_move_ids:
				stock_move_obj.action_assign(cr, uid, toassign_move_ids, context=context)
		return True

class stock_move(models.Model):
	_inherit = 'stock.move'

	def action_done(self, cr, uid, ids, context=None):
		""" Process completely the moves given as ids and if all moves are done, it will finish the picking.
		"""
		context = context or {}
		picking_obj = self.pool.get("stock.picking")
		quant_obj = self.pool.get("stock.quant")
		todo = [move.id for move in self.browse(cr, uid, ids, context=context) if move.state == "draft"]
		if todo:
			ids = self.action_confirm(cr, uid, todo, context=context)
		pickings = set()
		procurement_ids = []
		#Search operations that are linked to the moves
		operations = set()
		move_qty = {}
		for move in self.browse(cr, uid, ids, context=context):
			move_qty[move.id] = move.product_qty
			for link in move.linked_move_operation_ids:
				operations.add(link.operation_id)

		#Sort operations according to entire packages first, then package + lot, package only, lot only
		operations = list(operations)
		operations.sort(key=lambda x: ((x.package_id and not x.product_id) and -4 or 0) + (x.package_id and -2 or 0) + (x.lot_id and -1 or 0))

		for ops in operations:
			if ops.picking_id:
				pickings.add(ops.picking_id.id)
			main_domain = [('qty', '>', 0)]
			for record in ops.linked_move_operation_ids:
				move = record.move_id
				self.check_tracking(cr, uid, move, not ops.product_id and ops.package_id.id or ops.lot_id.id, context=context)
				prefered_domain = [('reservation_id', '=', move.id)]
				fallback_domain = [('reservation_id', '=', False)]
				fallback_domain2 = ['&', ('reservation_id', '!=', move.id), ('reservation_id', '!=', False)]
				prefered_domain_list = [prefered_domain] + [fallback_domain] + [fallback_domain2]
				dom = main_domain + self.pool.get('stock.move.operation.link').get_specific_domain(cr, uid, record, context=context)
				quants = quant_obj.quants_get_prefered_domain(cr, uid, ops.location_id, move.product_id, record.qty, domain=dom, prefered_domain_list=prefered_domain_list,
                                                          restrict_lot_id=move.restrict_lot_id.id, restrict_partner_id=move.restrict_partner_id.id, context=context)
				if ops.product_id:
					#If a product is given, the result is always put immediately in the result package (if it is False, they are without package)
					quant_dest_package_id  = ops.result_package_id.id
					ctx = context
				else:
					# When a pack is moved entirely, the quants should not be written anything for the destination package
					quant_dest_package_id = False
					ctx = context.copy()
					ctx['entire_pack'] = True
				quant_obj.quants_move(cr, uid, quants, move, ops.location_dest_id, location_from=ops.location_id, lot_id=ops.lot_id.id, owner_id=ops.owner_id.id, src_package_id=ops.package_id.id, dest_package_id=quant_dest_package_id, context=ctx)

				# Handle pack in pack
				if not ops.product_id and ops.package_id and ops.result_package_id.id != ops.package_id.parent_id.id:
					self.pool.get('stock.quant.package').write(cr, SUPERUSER_ID, [ops.package_id.id], {'parent_id': ops.result_package_id.id}, context=context)
				if not move_qty.get(move.id):
					raise osv.except_osv(_("Error"), _("The roundings of your Unit of Measures %s on the move vs. %s on the product don't allow to do these operations or you are not transferring the picking at once. ") % (move.product_uom.name, move.product_id.uom_id.name))
				move_qty[move.id] -= record.qty
		#Check for remaining qtys and unreserve/check move_dest_id in
		move_dest_ids = set()
		for move in self.browse(cr, uid, ids, context=context):
			move_qty_cmp = float_compare(move_qty[move.id], 0, precision_rounding=move.product_id.uom_id.rounding)
			if move_qty_cmp > 0:  # (=In case no pack operations in picking)
				main_domain = [('qty', '>', 0)]
				prefered_domain = [('reservation_id', '=', move.id)]
				fallback_domain = [('reservation_id', '=', False)]
				fallback_domain2 = ['&', ('reservation_id', '!=', move.id), ('reservation_id', '!=', False)]
				prefered_domain_list = [prefered_domain] + [fallback_domain] + [fallback_domain2]
				self.check_tracking(cr, uid, move, move.restrict_lot_id.id, context=context)
				qty = move_qty[move.id]
				print qty
				quants = quant_obj.quants_get_prefered_domain(cr, uid, move.location_id, move.product_id, qty, domain=main_domain, prefered_domain_list=prefered_domain_list, restrict_lot_id=move.restrict_lot_id.id, restrict_partner_id=move.restrict_partner_id.id, context=context)
				quant_obj.quants_move(cr, uid, quants, move, move.location_dest_id, lot_id=move.restrict_lot_id.id, owner_id=move.restrict_partner_id.id, context=context)

			# If the move has a destination, add it to the list to reserve
			if move.move_dest_id and move.move_dest_id.state in ('waiting', 'confirmed'):
				move_dest_ids.add(move.move_dest_id.id)

			if move.procurement_id:
				procurement_ids.append(move.procurement_id.id)

			#unreserve the quants and make them available for other operations/moves
			quant_obj.quants_unreserve(cr, uid, move, context=context)
		# Check the packages have been placed in the correct locations
		self._check_package_from_moves(cr, uid, ids, context=context)
		#set the move as done
		self.write(cr, uid, ids, {'state': 'done', 'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)}, context=context)
		self.pool.get('procurement.order').check(cr, uid, procurement_ids, context=context)
		#assign destination moves
		if move_dest_ids:
			self.action_assign(cr, uid, list(move_dest_ids), context=context)
		#check picking state to set the date_done is needed
		done_picking = []
		for picking in picking_obj.browse(cr, uid, list(pickings), context=context):
			if picking.state == 'done' and not picking.date_done:
				done_picking.append(picking.id)
		if done_picking:
			picking_obj.write(cr, uid, done_picking, {'date_done': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)}, context=context)
		return True